# larellan-bbtautau

Small projects I've made for the di-higgs boosted bbtautau analysis.

## AccxEff

Generates Acceptance times Efficiency plots for different MC signals
in a given kinematic region, based on thrown and reconstructed number
of entries.

## Xhh_sensitivity

Finds maximum sensitivity (defined as sig/sqrt(bkg)) region for
different values of m_bb, m_tautau and Xhh.