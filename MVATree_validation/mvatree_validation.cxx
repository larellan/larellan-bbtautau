bool m_debug = false;

void initialize(TH1F * & hist, TString name);
void draw_variable( TH1F * & bkg_hist, TH1F * & sig_hist, TH1F * & dat_hist);

void mvatree_validation()
{
  TFile output_file("/home/luciano/Physics/ATLAS/v10_validation/bowen_limits.root","recreate");
  std::vector<string> sig_filenames = {
    "Xtohh1000",
    "Xtohh1200",
    "Xtohh1400",
    "Xtohh1600",
    "Xtohh1800",
    "Xtohh2000",
    "Xtohh2500",
    "Xtohh3000",
  };

  std::vector<string> bkg_filenames =
    {
     "ttbar",//
     "stopWt",//
     "stopschan",//
     "stoptchan",//
     "Wtaunu_221",//
     "WW_Pw",//
     "WZ_Pw",//
     "Zee_221",//
     //"Ztautau_221",
     "ZZ_Pw",//
     "Zmumu_Pw",//
     "Ztautau",
     //"Wenu", // 0 entries in Nominal
     // "Wmumu_Pw",// doesnt have Nominal
    };
  
  std::vector<string> sig_samples =
    {
     /*
     "Hhhbbtautau1000",
     "Hhhbbtautau1200",
     "Hhhbbtautau1400",
     "Hhhbbtautau1600",
     "Hhhbbtautau1800",
     "Hhhbbtautau2000",
     "Hhhbbtautau2500",
     "Hhhbbtautau3000",
     */
     // David XSection file: Xtohh*_Hw
     "Xtohh1000_Hw",
     "Xtohh1200_Hw",
     "Xtohh1400_Hw",
     "Xtohh1600_Hw",
     "Xtohh1800_Hw",
     "Xtohh2000_Hw",
     "Xtohh2500_Hw",
     "Xtohh3000_Hw",
    };
  
  std::vector<string> bkg_samples =
    {
     "fakes",
     "stops",// empty in the latest apparently
     "stopt",
     "stopWt",
     "ttbar",
     "ttbar_nonallhad",//new in october
     "ttbar_allhad",//new in october
     "Zbb",
     "Zbl",
     "Zc",
     "Zcc",
     "Zcl",
     "Zl",
     "Zbc",// new in october (?)
     "Zee_Sh221",// new in october (?)
     "WWPw",
     "WZPw",
     "ZZPw",
     "Wbc",// new in october (?)
     "Wbb",
     "Wbl",
     "Wc",
     "Wcc",
     "Wcl",
     "Wl",     
    };
  
  
  std::vector<const char *> dirs;
  const char * combined = "/home/luciano/Physics/ATLAS/data/MVATree_v10_1017";
  dirs.push_back(combined);
  TString sig_files[100];
  TString bkg_files[100];
  TString dat_files[100];
  Int_t n = 0;
  Int_t sig_number = 0;
  Int_t bkg_number = 0;
  Int_t dat_number = 0;
  for ( auto directory: dirs ){
    char* dir = gSystem->ExpandPathName(directory);
    void* dirp = gSystem->OpenDirectory(dir);
    const char* entry;
    TString str;
    while((entry = (char*)gSystem->GetDirEntry(dirp))) {
      str = entry;
      // separate signals and bkg
      for ( int i = 0; i < sig_filenames.size(); i++ ) {
	if (str.Contains(sig_filenames.at(i)))
	  sig_files[sig_number++] = gSystem->ConcatFileName(dir, entry);
      }
      for ( int i = 0; i < bkg_filenames.size(); i++ ) {
	if (str.Contains(bkg_filenames.at(i)))
	  bkg_files[bkg_number++] = gSystem->ConcatFileName(dir, entry);
      }
      if ( str.Contains("data")) {
	dat_files[dat_number++] = gSystem->ConcatFileName(dir,entry);
      }
    }
  }
  // check files
  if ( m_debug ){
    for ( int i = 0; i < sig_number; i++ ){
      cout << "sig file " << sig_files[i] << "\t" << i << endl;
    }
    for ( int i = 0; i < bkg_number; i++ ){
      cout << "bkg file " << bkg_files[i] << "\t" << i << endl;
    }
    for ( int i = 0; i < dat_number; i++ ){
      cout << "dat file " << dat_files[i] << "\t" << i << endl;
    }
  }
  
  
  // files are in, now get into them
  
  // note: the c++ 'find' method, compares EQUAL
  // so it won't find QCDCR "in" QCDCR_Pre2DMassCut
  std::vector<string> region =
    {
     "SR_Pre2DMassCut",
     "QCDCR_Pre2DMassCut",
     "SR",
     "QCDCR",
    };
  
  std::vector<string> btag =
    {
     "0tag",
     "1tag",
     "2tag",
    };
  
  std::vector<string> variable =
    {
     "SelDTsubsEta",
     "SelDTsubsM",
     "SelDTsubsPhi",
     "SelDTsubsPt",
     "SelFJEta",
     "SelFJM",
     "SelFJPhi",
     "SelFJPt",
     "MET",
     "dPhi_DTsubs_MET",
     //"SelFTTruthMatch",
     "subsmhh",
     "subspthh"
    };

  std::map<string,int> var_map;
  for ( int i = 0; i < variable.size(); i++ ) {
    var_map.insert({variable[i],i});
  }
  std::map<string,int> reg_map;
  for ( int i = 0; i < region.size(); i++ ) {
    reg_map.insert({region[i],i});
  }
  std::map<string,int> bsam_map;
  for ( int i = 0; i < bkg_samples.size(); i++ ) {
    bsam_map.insert({bkg_samples[i],i});
  }
  std::map<string,int> ssam_map;
  for ( int i = 0; i < sig_samples.size(); i++ ) {
    ssam_map.insert({sig_samples[i],i});
  }
  

      
    

  TH1F * bkg_histarray[region.size()][btag.size()][variable.size()][bkg_samples.size()];
  TH1F * sig_histarray[region.size()][btag.size()][variable.size()][sig_samples.size()];
  TH1F * dat_histarray[region.size()][btag.size()][variable.size()];

  // initialize histograms
  // has to be done by hand I guess
  for ( int i = 0; i < region.size(); i++ ) {
    for ( int j = 0; j < btag.size(); j++ ) {
      for ( int k = 0; k < variable.size(); k++ ) {
	TString base = "data_"+btag.at(j)+"2pjet_0ptv_"+region.at(i)+"_"+variable.at(k);
	initialize(dat_histarray[i][j][k],base);
	for ( int l = 0; l < bkg_samples.size(); l++ ){
	  TString base_bkg = bkg_samples.at(l)+"_"+btag.at(j)+"2pjet_0ptv_"+region.at(i)+"_"+variable.at(k);
	  initialize(bkg_histarray[i][j][k][l], base_bkg);
	}
	for ( int l = 0; l < sig_samples.size(); l++ ){
	  TString base_sig = sig_samples.at(l)+"_"+btag.at(j)+"2pjet_0ptv_"+region.at(i)+"_"+variable.at(k);
	  initialize(sig_histarray[i][j][k][l], base_sig);
	}
      }
    }
  }

  // fill bkg histograms
  for ( int i = 0; i < bkg_number; i++ ) {
    TFile *f = new TFile(bkg_files[i]);
    TTreeReader nom("Nominal",f);
    TTreeReaderValue<std::string>   sample(nom,"sample");
    TTreeReaderValue<Float_t>       weight(nom,"EventWeight");
    TTreeReaderValue<Float_t> weightnoxsec(nom,"EventWeightNoXSec");
    TTreeReaderValue<std::string>   region(nom,"m_region");
    TTreeReaderValue<Int_t>          nbtag(nom,"m_FJNbtagJets");
    // FJ
    TTreeReaderValue<Float_t>         fjpt(nom,"m_FJpt");
    TTreeReaderValue<Float_t>          fjm(nom,"m_FJm");
    TTreeReaderValue<Float_t>        fjeta(nom,"m_FJeta");
    TTreeReaderValue<Float_t>        fjphi(nom,"m_FJphi");
    // DT
    TTreeReaderValue<Float_t>         dtpt(nom,"m_DTpt");
    TTreeReaderValue<Float_t>          dtm(nom,"m_DTm");
    TTreeReaderValue<Float_t>        dteta(nom,"m_DTeta");
    TTreeReaderValue<Float_t>        dtphi(nom,"m_DTphi");
    // MISC
    TTreeReaderValue<Float_t>   dphiDTwMET(nom,"m_dPhiDTwMET");
    TTreeReaderValue<Float_t>          MET(nom,"m_MET");
    TTreeReaderValue<Float_t>          hhm(nom,"m_hhm");
    TTreeReaderValue<Float_t>         hhpt(nom,"m_bbttpt");

    std::map<string,TTreeReaderValue<Float_t>> mvatree_map;
    mvatree_map.insert({"SelDTsubsEta",dteta});
    mvatree_map.insert({"SelDTsubsM"  ,dtm});
    mvatree_map.insert({"SelDTsubsPhi",dtphi});
    mvatree_map.insert({"SelDTsubsPt" ,dtpt});
    mvatree_map.insert({"SelFJEta"    ,fjeta});
    mvatree_map.insert({"SelFJM"      ,fjm});
    mvatree_map.insert({"SelFJPhi"    ,fjphi});
    mvatree_map.insert({"SelFJPt"     ,fjpt});
    mvatree_map.insert({"MET"         ,MET});
    mvatree_map.insert({"dPhi_DTsubs_MET",dphiDTwMET});
    mvatree_map.insert({"subsmhh"     ,hhm});
    mvatree_map.insert({"subspthh"    ,hhpt});

    while ( nom.Next() ) {
      for ( int v = 0; v < variable.size(); v++ ) {
	float value = *mvatree_map.find(variable[v])->second;
	TString aux = variable[v];
	if ( aux.EndsWith("M") ){
	  value /= 1000.0;
	}
	bkg_histarray[reg_map.find(*region)->second][*nbtag][v][bsam_map.find(*sample)->second]->Fill(value,*weight);
      }
    }
    delete f;
  } // end fill bkg histograms
  
  // fill SIG histograms
  for ( int i = 0; i < sig_number; i++ ) {
    TFile *f = new TFile(sig_files[i]);
    TTreeReader nom("Nominal",f);
    TTreeReaderValue<std::string>   sample(nom,"sample");
    TTreeReaderValue<Float_t>       weight(nom,"EventWeight");
    TTreeReaderValue<Float_t> weightnoxsec(nom,"EventWeightNoXSec");
    TTreeReaderValue<std::string>   region(nom,"m_region");
    TTreeReaderValue<Int_t>          nbtag(nom,"m_FJNbtagJets");
    // FJ
    TTreeReaderValue<Float_t>         fjpt(nom,"m_FJpt");
    TTreeReaderValue<Float_t>          fjm(nom,"m_FJm");
    TTreeReaderValue<Float_t>        fjeta(nom,"m_FJeta");
    TTreeReaderValue<Float_t>        fjphi(nom,"m_FJphi");
    // DT
    TTreeReaderValue<Float_t>         dtpt(nom,"m_DTpt");
    TTreeReaderValue<Float_t>          dtm(nom,"m_DTm");
    TTreeReaderValue<Float_t>        dteta(nom,"m_DTeta");
    TTreeReaderValue<Float_t>        dtphi(nom,"m_DTphi");
    // MISC
    TTreeReaderValue<Float_t>   dphiDTwMET(nom,"m_dPhiDTwMET");
    TTreeReaderValue<Float_t>          MET(nom,"m_MET");
    TTreeReaderValue<Float_t>          hhm(nom,"m_hhm");
    TTreeReaderValue<Float_t>         hhpt(nom,"m_bbttpt");

    std::map<string,TTreeReaderValue<Float_t>> mvatree_map;
    mvatree_map.insert({"SelDTsubsEta",dteta});
    mvatree_map.insert({"SelDTsubsM"  ,dtm});
    mvatree_map.insert({"SelDTsubsPhi",dtphi});
    mvatree_map.insert({"SelDTsubsPt" ,dtpt});
    mvatree_map.insert({"SelFJEta"    ,fjeta});
    mvatree_map.insert({"SelFJM"      ,fjm});
    mvatree_map.insert({"SelFJPhi"    ,fjphi});
    mvatree_map.insert({"SelFJPt"     ,fjpt});
    mvatree_map.insert({"MET"         ,MET});
    mvatree_map.insert({"dPhi_DTsubs_MET",dphiDTwMET});
    mvatree_map.insert({"subsmhh"     ,hhm});
    mvatree_map.insert({"subspthh"    ,hhpt});

    while ( nom.Next() ) {
      for ( int v = 0; v < variable.size(); v++ ) {
	float value = *mvatree_map.find(variable[v])->second;
	TString aux = variable[v];
	if ( aux.EndsWith("M") ){
	  value /= 1000.0;
	}
	sig_histarray[reg_map.find(*region)->second][*nbtag][v][ssam_map.find(*sample)->second]->Fill(value,*weight);
      }
    }
    delete f;
  } // end fill SIG histograms
  
    // fill DATA histograms
  for ( int i = 0; i < dat_number; i++ ) {
    TFile *f = new TFile(dat_files[i]);
    TTreeReader nom("Nominal",f);
    TTreeReaderValue<std::string>   sample(nom,"sample");
    TTreeReaderValue<Float_t>       weight(nom,"EventWeight");
    TTreeReaderValue<Float_t> weightnoxsec(nom,"EventWeightNoXSec");
    TTreeReaderValue<std::string>   region(nom,"m_region");
    TTreeReaderValue<Int_t>          nbtag(nom,"m_FJNbtagJets");
    // FJ
    TTreeReaderValue<Float_t>         fjpt(nom,"m_FJpt");
    TTreeReaderValue<Float_t>          fjm(nom,"m_FJm");
    TTreeReaderValue<Float_t>        fjeta(nom,"m_FJeta");
    TTreeReaderValue<Float_t>        fjphi(nom,"m_FJphi");
    // DT
    TTreeReaderValue<Float_t>         dtpt(nom,"m_DTpt");
    TTreeReaderValue<Float_t>          dtm(nom,"m_DTm");
    TTreeReaderValue<Float_t>        dteta(nom,"m_DTeta");
    TTreeReaderValue<Float_t>        dtphi(nom,"m_DTphi");
    // MISC
    TTreeReaderValue<Float_t>   dphiDTwMET(nom,"m_dPhiDTwMET");
    TTreeReaderValue<Float_t>          MET(nom,"m_MET");
    TTreeReaderValue<Float_t>          hhm(nom,"m_hhm");
    TTreeReaderValue<Float_t>         hhpt(nom,"m_bbttpt");

    std::map<string,TTreeReaderValue<Float_t>> mvatree_map;
    mvatree_map.insert({"SelDTsubsEta",dteta});
    mvatree_map.insert({"SelDTsubsM"  ,dtm});
    mvatree_map.insert({"SelDTsubsPhi",dtphi});
    mvatree_map.insert({"SelDTsubsPt" ,dtpt});
    mvatree_map.insert({"SelFJEta"    ,fjeta});
    mvatree_map.insert({"SelFJM"      ,fjm});
    mvatree_map.insert({"SelFJPhi"    ,fjphi});
    mvatree_map.insert({"SelFJPt"     ,fjpt});
    mvatree_map.insert({"MET"         ,MET});
    mvatree_map.insert({"dPhi_DTsubs_MET",dphiDTwMET});
    mvatree_map.insert({"subsmhh"     ,hhm});
    mvatree_map.insert({"subspthh"    ,hhpt});

    while ( nom.Next() ) {
      for ( int v = 0; v < variable.size(); v++ ) {
	float value = *mvatree_map.find(variable[v])->second;
	TString aux = variable[v];
	if ( aux.EndsWith("M") ){
	  value /= 1000.0;
	}
	if ( *sample == "data" ){
	  dat_histarray[reg_map.find(*region)->second][*nbtag][v]->Fill(value,*weight);
	} else if ( *sample == "fakes" ){
	  bkg_histarray[reg_map.find(*region)->second][*nbtag][v][bsam_map.find(*sample)->second]->Fill(value,*weight);
	}
      }
    }
    delete f;
  } // end fill DATA histograms

  if ( m_debug ){
    for ( int i = 0; i < region.size(); i++ ) {
      for ( int j = 0; j < btag.size(); j++ ) {
	for ( int k = 0; k < variable.size(); k++ ) {
	  if ( k != 2 ){
	    continue;
	  }
	  TString base = "data_"+btag.at(j)+"_"+region.at(i)+"_"+variable.at(k);
	  cout << dat_histarray[i][j][k]->GetEntries() << "\t" << base << endl;
	  for ( int l = 0; l < bkg_samples.size(); l++ ){
	    TString base_bkg = bkg_samples.at(l)+"_"+btag.at(j)+"_"+region.at(i)+"_"+variable.at(k);
	    cout << bkg_histarray[i][j][k][l]->GetEntries() << "\t" << base_bkg << endl;
	  }
	  for ( int l = 0; l < sig_samples.size(); l++ ){
	    TString base_sig = sig_samples.at(l)+"_"+btag.at(j)+"_"+region.at(i)+"_"+variable.at(k);
	    cout << sig_histarray[i][j][k][l]->GetEntries() << "\t" <<  base_sig << endl;
	  }
	}
      }
    }
  }

  
  output_file.Write();
  
} // END MAIN



void initialize(TH1F * & hist, TString name)
{
  int nbins(100);
  float low(0), high(10);
  // axes name
  TObjArray *var_token = name.Tokenize("_");
  int size = var_token->GetEntries();
  TString var ;
  if ( !name.Contains("dPhi_DTsubs_MET") ) {
    var = ((TObjString *)(var_token->At(size-1)))->String();
  } else {
    var = "dPhi_DTsubs_MET";
  }
  if ( name.EndsWith("Eta")) {
    nbins = 60;
    low   = -3.0;
    high  =  3.0;
  } else if ( name.EndsWith("Phi") ) {
    nbins = 80;
    low   = -4.0;
    high  =  4.0;
  } else if ( name.EndsWith("Pt") ) {
    nbins =  200;
    low   = 0;
    high  = 2000;
  } else if ( name.EndsWith("M") ) {
    nbins = 200;
    low   = 0;
    high  = 1000;
  } else if ( var.EqualTo("MET") ) {
    nbins = 100;
    low   = 0;
    high  = 1000;
  } else if ( var.BeginsWith("dPhi") ) {
    nbins = 64;
    low   = -3.2;
    high  =  3.2;
  } else if ( name.Contains("TruthMatch") ) {
    nbins = 2;
    low   = -0.5;
    high  = 1.5;
  } else if ( var.EqualTo("subsmhh") || var.EqualTo("subspthh") ) {
    nbins = 400;
    low   = 0;
    high  = 4000;
  } 
  hist = new TH1F(name,name,nbins,low,high);
  hist->GetYaxis()->SetTitle("Events");
  hist->GetXaxis()->SetTitle(var);
}

void draw_variable( TH1F * & bkg_hist, TH1F * & sig_hist, TH1F * & dat_hist)
{
  cout << "what" << endl;
}
