#include "TFile.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TTreeReaderArray.h"
#include "TTreeReaderValue.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TEllipse.h"

TString ATLASDIR = "/home/luciano/Physics/ATLAS/";



double truth(TString histname){
  if ( histname.Contains("1000") )
    return 360000.0;
  else
    return 190000.0;
}


// exports graph
// must include the full path in the output filename
void export_graph(TGraph * h2, TString out_filename) {
  auto c1 = new TCanvas();
  c1->SetCanvasSize(1000,1000);
  c1->SetLeftMargin(0.12);
  c1->SetRightMargin(0.15);
  h2->SetLineWidth(3);
  h2->SetMarkerSize(3);
  h2->SetMarkerStyle(kFullSquare);
  h2->Draw("PAL PLC");
  c1->SaveAs(out_filename);
  c1->Modified();
  c1->Update();
  delete c1;
}


std::vector<TGraph *> fill_graphs(int regions,
				  int nbtag,
				  int n,
				  double *x,
				  double Y[][3][8])
{
  std::vector<TGraph *> v;
  for ( int i = 0; i < regions ; i++ ){
    TGraph * gr_same = new TGraph(n,x,Y[i][nbtag]);
    v.push_back(gr_same);
  }
  return v;
}



void combined_graph(std::vector<TGraph *> v,
		    TString btag,
		    std::vector<std::string> regions,
		    int reg_ini,
		    int reg_end)
{
  
  auto c0 = new TCanvas();
  c0->SetCanvasSize(3000,2000);
  auto mg0 = new TMultiGraph();
  for ( int i = reg_ini; i < reg_end; i++ ){
    int j = i-reg_ini;
    mg0->Add(v[i]);
    v[i]->SetLineWidth(4);
    v[i]->SetMarkerSize(3);
    v[i]->SetMarkerStyle(20+j);
    TString tit = regions[i];
    v[i]->SetTitle(tit);
  }
  TString mgtitle = "; Signal mass [GeV]; Acceptance x Efficiency";

  mg0->SetTitle(mgtitle);
  mg0->SetMinimum(0.0);
  if ( btag == "0tag" )
    mg0->SetMaximum(1.0);
  else
    mg0->SetMaximum(0.16);
  gStyle->SetPalette(55);
  mg0->Draw("PAL PMC PLC");
  
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendTextSize(.02);
  c0->BuildLegend(0.65,0.7,0.9,0.89,"Cuts","clp");
  TString out_filename
    = ATLASDIR+"AccxEff_plots/combined_"+btag+".png";
  c0->SaveAs(out_filename);
  delete c0;
  delete mg0;
}



void AccxEff()
{
  gStyle->SetOptStat(0);
  gStyle->SetPaintTextFormat("4.3f");
  std::vector<string> sig_samples = {
    "Hhhbbtautau1000",
    "Hhhbbtautau1200",
    "Hhhbbtautau1400",
    "Hhhbbtautau1600",
    "Hhhbbtautau1800",
    "Hhhbbtautau2000",
    "Hhhbbtautau2500",
    "Hhhbbtautau3000"
  };

  Double_t x[8] =
    { 1000.0, 1200.0, 1400.0, 1600.0, 1800.0, 2000.0, 2500.0, 3000.0 };
  // sample energy ( x axis )
  // should be set automatically from sig_samples, not manually like here
  
  Int_t n = sig_samples.size();

  std::vector<string> prebtag_reg = {
    "PrePreSel",
    "Initial",
    "10GeVMETCut",
    "neitherGoodOrFakeDitau",
    "SelFJNULL",
  };

  std::vector<string> postbtag_reg =
    {
     "SR",
    };

  std::vector<string> regions;
  for ( auto reg: prebtag_reg ) regions.push_back(reg);
  for ( auto reg: postbtag_reg ) regions.push_back(reg);

  std::vector<string> regions_description =
    {
     "Maker selection",
     "Trigger",
     "MET > 10 GeV",
     "DiTau selection",
     "Large-R jet selection",
     "Signal region"
    };
  

    "0tag",
    "1tag",
    "2tag"
  };

  // output directory
  TString out_path = ATLASDIR+"AccxEff_plots";

  // input directory
  TFile *f
    = new TFile(ATLASDIR+"data/v10_accxeff_0901_v2.root");
  double Y[20][3][8]; // Y[regions][nbtag][signal_samples], max of 20 regions
  int region_counter = -1;
  for ( auto ireg: regions ) {
    region_counter++;
    double y_multitag[100];
    for ( int i = 0; i < n; i++ )
      y_multitag[i] = 0;
    int btag_counter;
    for ( auto itag: btag ) {
      if ( itag == "0tag" )
	btag_counter = 0;
      if ( itag == "1tag" )
	btag_counter = 1;
      if ( itag == "2tag" )
	btag_counter = 2;
      double y[100];
      int sigcounter = 0;
      for ( auto isig: sig_samples ) {
	for ( auto&& keyAsObj : *f->GetListOfKeys() ) {
	  auto key = (TKey *) keyAsObj;
	  TString clase = key->GetClassName();
	  
	  if ( !clase.Contains("TH1") )
	    continue;
	  TString histname = key->GetName();
	  
	  if ( !histname.Contains(ireg) ||
	       !histname.Contains(itag) ||
	       !histname.Contains(isig) ||
	       ( !histname.Contains("subsmhh") &&
		 !histname.Contains("unweighted") )
	       ) {
	    continue;
	  }
	  TH1 *htemp;
	  htemp = (TH1*)key->ReadObj();
	  double reco_selected =  htemp->GetEntries();
	  double truth_generated = truth(histname);
	  y[sigcounter] = reco_selected/truth_generated;
	  Y[region_counter][btag_counter][sigcounter]
	    = reco_selected/truth_generated;
	  y_multitag[sigcounter] += reco_selected/truth_generated;
	  sigcounter++;
	}
      }
    }
  }
  
  // Y[regions][nbtag][signal_samples]
  // makes vector with TGraphs for all regions for a given btag number
  std::vector<TGraph *> v0tag = fill_graphs(regions.size(),0,n,x,Y);  
  std::vector<TGraph *> v1tag = fill_graphs(regions.size(),1,n,x,Y);
  std::vector<TGraph *> v2tag = fill_graphs(regions.size(),2,n,x,Y);

  
  
  int prebtag = prebtag_reg.size();
  // makes final combined plot (TMultigraph) for a given btag number
  combined_graph(v0tag,btag[0],regions_description, 0,prebtag);
  combined_graph(v1tag,btag[1],regions_description,prebtag,regions.size());
  TGraph * gr_sr1 = new TGraph(n,x,Y[5][1]);
  TGraph * gr_sr2 = new TGraph(n,x,Y[5][2]);
    
  std::vector<TGraph *> comb;
  comb.push_back(gr_sr1);
  comb.push_back(gr_sr2);

  //combined_graph(comb,btag[2],btag,1,3);
  auto c0 = new TCanvas();
  c0->SetCanvasSize(3000,2000);
  auto mg0 = new TMultiGraph();

  mg0->Add(comb[0]);
  comb[0]->SetLineWidth(4);
  comb[0]->SetMarkerSize(3);

  comb[0]->SetMarkerStyle(20+0);
  TString tit = "1 tag";
  comb[0]->SetTitle(tit);

  mg0->Add(comb[1]);
  comb[1]->SetLineWidth(4);
  comb[1]->SetMarkerSize(3);

  comb[1]->SetMarkerStyle(20+1);
  TString tit2 = "2 tag";
  comb[1]->SetTitle(tit2);

  comb[0]->SetLineColor(2);
  comb[0]->SetMarkerColor(2);
  comb[1]->SetLineColor(4);
  comb[1]->SetMarkerColor(4);

  
  TString mgtitle = "; Signal mass [GeV]; Acceptance x Efficiency";
 
  mg0->SetTitle(mgtitle);
  mg0->SetMinimum(0.0);
  mg0->SetMaximum(0.16);
  mg0->Draw("PAL");
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendTextSize(.02);
  c0->BuildLegend(0.65,0.7,0.9,0.89,"","clp");
  TString out_filename
    = ATLASDIR+"AccxEff_plots/combined_"+".png";
  c0->SaveAs(out_filename);
  delete c0;
  delete mg0;
  //cout << Y[0][1][7] << " " << Y[0][2][7] << " " << Y[3][2][7] << endl;
  cout << "finished correctly" << endl;
}
