#include "TFile.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TTreeReaderArray.h"
#include "TTreeReaderValue.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TEllipse.h"


//Float_t gXsection = 14.5879552; // git cross section file
Float_t gXsection = 1.0; // david's cross section file (use this one)

//////////////////////////////////
//////////////////////////////////
// GLOBAL VARIABLES for the script
//////////////////////////////////
//////////////////////////////////
bool m_debug = false;
TString gBinning = "medium";
TString gShape   = "vert"; // elip or poly or vert or square
TString gBtag    = "2tag"; // e.g. "1", "12", "1 or 2 tag", "0,1,2", will appear in filename
bool    gDPhiInclusive  = false;
bool    gMultipleWindows = true;
TString gMassWindow = "high"; // high or low
int gSignificance_formula = 1;
// Xhh values to probe
Float_t gXhh_min  = 0.5;
Float_t gXhh_max  = 5.0;
Float_t gXhh_step = 0.01;

Float_t gbb_center_fixed = 122.5;
Float_t gtt_center_fixed =  81.0;
//Float_t gtt_center_fixed =  95.0;
// denominator values for vertical ellipse
Float_t gVertbb = 0.1;
Float_t gVerttt = 0.25;
// square cuts on bbtt
Float_t gSquare_bb_max = 160;
Float_t gSquare_bb_min = 60;
Float_t gSquare_tt_min = 50;
Float_t gSquare_tt_max = 130;
// Di tau mass
// m_DTm        : visible
// m_DTm_Eff    : effective
// m_DTm_ConVis : constrained visible
// m_DTmConEff : constrained effective
TString gDTM = "m_DTm";
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
// INPUT AND OUTPUT DIRECTORIES
/////////////////////////////////
// input:
//const char * combined = "/home/luciano/Physics/ATLAS/data/MVATree_v10_0916";
const char * combined = "/home/luciano/Physics/ATLAS/data/MVATree_v10_1017";
// output:
TString out_path = "/home/luciano/Physics/ATLAS/Xhh_plots/bin_"+gBinning+"/";
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
// FUNCTIONS DECLARATION
Float_t errorbb(Float_t x) {
  return 0.1*x;
}

Float_t errortt(Float_t x) {
  return 0.1*x;
}

Float_t Xhh(Float_t bb_center, Float_t tt_center, Float_t bb, Float_t tt, TString shape = gShape) {
  if ( shape == "elip" ) {
    return sqrt(pow((bb_center-bb)/errorbb(bb_center),2) + pow((tt_center-tt)/errortt(tt_center),2));
  } else if ( shape == "poly" ) {
    return sqrt(pow((bb_center-bb)/errorbb(bb),2) + pow((tt_center-tt)/errortt(tt),2));
  } else if ( shape == "vert" ) {
    return sqrt(pow((bb_center-bb)/(gVertbb*bb_center),2) + pow((tt_center-tt)/(gVerttt*tt_center),2));
  } else if ( shape == "square" ) {
    if ( bb > gSquare_bb_max ||
	 bb < gSquare_bb_min ||
	 tt > gSquare_tt_max ||
	 tt < gSquare_tt_min ) {
      return 0;
    } else if ( bb > gSquare_bb_min &&
		bb < gSquare_bb_max &&
		tt > gSquare_tt_min &&
		tt < gSquare_tt_max ) {
      return gXhh_max + 1.0;
    } else {
      return gXhh_max + 1.0;
      cout << "ERROR: I don't know how did the script get to this point!" << endl;
    }
  } else if ( shape == "none" ) {
    return 0;
  } else {
    cout << "ERROR: calculating Xhh, please set shape to one of the accepted values" << endl;
    return 0;
  }
}

void draw_shape(Float_t, Float_t, Float_t,TString shape = gShape);
void export_hist(TH2 *, TString, TString options = "colz");
void export_hist_elip(TH2 *, TString, Float_t, Float_t, Float_t, TString shape = gShape);
std::vector<std::vector<Float_t>> Optimum (
					   std::vector<TH2F> sig_hist_vector,
					   TH2 * bkg_hist,
					   std::vector<std::vector<Float_t>> optimum_per_signal,
					   TString shape = gShape
					   );

std::vector<std::vector<Float_t>> Fixed(
				       std::vector<TH2F>sig_hist_vector,
				       TH2 * bkg_hist,
				       std::vector<std::vector<Float_t>> optimum_per_signal,
				       TString shape = gShape
				       );

void Compare_shape_and_best_vert(
				 std::vector<TH2F> sig_hist_vector,
				 std::vector<std::vector<Float_t>> optimum_per_signal,
				 std::vector<std::vector<Float_t>> optimum_fixed,
				 TString shape = gShape
				 );


Float_t Significance(Float_t sig, Float_t bkg) {
  if ( gSignificance_formula == 0 ) {
    return sig/sqrt(bkg);
  } else if ( gSignificance_formula == 1 ) {
    return sqrt( 2*(sig+bkg)*log( 1 + (sig/bkg) ) - 2*sig );
  } else {
    cout << "Error calculating significance, "
	 << "please set gSignificance_formula to an appropiate value" << endl;
    return 0;
  }
}

//////////////////////////////////
//////////////////////////////////
///////// MAIN
//////////////////////////////////
//////////////////////////////////
void Xhh_optimization()
{
  gStyle->SetOptStat(0);
  gStyle->SetPaintTextFormat("4.3f");


  ofstream full_output_file[20];

  std::vector<string> sig_samples =
    {/*
     "Xtohh1000",
     "Xtohh1200",
     "Xtohh1400",
     "Xtohh1600",
     "Xtohh1800",*/
     "Xtohh2000",/*
     "Xtohh2500",
     "Xtohh3000",*/
    };
  
  std::vector<string> bkg_samples =
    {
     "ttbar",
     "stopWt",
     "stopschan",
     "stoptchan",
     "Wtaunu",
     "WW_Pw",
     "WZ_Pw",
     "Zee_221",
     "Ztautau_221",
     "ZZ_Pw",
     "Zmumu_Pw",
     "Ztautau",
     "data", // need to get fakes from data
    };
  
    
  // mass binning, bbtautau
  int bin_n, bin_min, bin_max;
  if ( gBinning == "coarse" ){ //20 GeV bins
    bin_n   = 20;
    bin_min = 0;
    bin_max = 400;
  } else if ( gBinning == "medium" ){ //5 GeV bins (as in bbbb)
    bin_n   = 80;
    bin_min = 0;
    bin_max = 400;
  } else if ( gBinning == "fine" ){//2 GeV bins
    bin_n   = 200;
    bin_min = 0;
    bin_max = 400;
  }

  TString full_output_file_filename[20];

  std::vector<const char *> dirs;
  dirs.push_back(combined);
  TString sig_files[100];
  TString bkg_files[100];
  Int_t n = 0;
  Int_t sig_number = 0;
  Int_t bkg_number = 0;
  for ( auto directory: dirs ){
    char* dir = gSystem->ExpandPathName(directory);
    void* dirp = gSystem->OpenDirectory(dir);
    const char* entry;
    TString str;
    while((entry = (char*)gSystem->GetDirEntry(dirp))) {
      str = entry;
      //cout << "entry " << entry << endl;
      // separate signals and bkg
      for ( int i = 0; i < sig_samples.size(); i++ ) {
	if (str.Contains(sig_samples.at(i))){
	  sig_files[sig_number++] = gSystem->ConcatFileName(dir, entry);
	  //cout << sig_number << " " << sig_files[sig_number-1] << endl;
	}
      }
      for ( int i = 0; i < bkg_samples.size(); i++ ) {
	if (str.Contains(bkg_samples.at(i)))
	  bkg_files[bkg_number++] = gSystem->ConcatFileName(dir, entry);
      }
    }
  }

  // check files
  if ( m_debug ){
    for ( int i = 0; i < sig_number; i++ ) {
      cout << "sig file " << sig_files[i] << "\t" << i << endl;
    }
    for ( int i = 0; i < bkg_number; i++ ) {
      cout << "bkg file " << bkg_files[i] << "\t" << i << endl;
    }
  }

  TH2 * bkg_hist = new TH2F("bkg_hist",
			    "Background sum "+gBtag+";m_{bb};m_{#tau#tau}",
			    bin_n,bin_min,bin_max,bin_n,bin_min,bin_max);
  TH2 * bkg_hist_2tag = new TH2F("bkg_hist_2tag",
				 "Background sum, 2-tag;m_{bb};m_{#tau#tau}",
				 bin_n,bin_min,bin_max,bin_n,bin_min,bin_max);

  // BACKGROUND HISTOGRAM
  for ( int i = 0; i < bkg_number; i++ ) {
    TFile *f = new TFile(bkg_files[i]);
    TTreeReader nom("Nominal",f);
    TTreeReaderValue<Float_t> weight(nom,"EventWeight");

    TTreeReaderValue<std::string> reg(nom,"m_region");
    TTreeReaderValue<std::string> sample(nom,"sample");
    TTreeReaderValue<Float_t> mbb(nom,"m_FJm");
    TTreeReaderValue<Float_t> mtautau(nom,gDTM);
    TTreeReaderValue<Int_t> nbtag(nom,"m_FJNbtagJets");
    
    while ( nom.Next() ) {
      if ( *sample == "data" ){
	continue;
      }
      TString btag = Form("%i",*nbtag);
      if ( !gBtag.Contains(btag) ){
	continue;
      }
      if ( *reg == "SR_Pre2DMassCut" || gDPhiInclusive )
	bkg_hist->Fill(*mbb/1000.0,*mtautau/1000.0,*weight);
    }
    delete f;
  }
  TString bkg_export = out_path+"bkg_sum_"+gBtag+".png";
  cout << "BKG INTEGRAL: " << bkg_hist->Integral() << endl;
  export_hist(bkg_hist,bkg_export);

  cout << "SIG NUMBER: " << sig_number << endl;


  // SIGNAL HISTOGRAMS
  std::vector< TH2F > sig_hist_vector;
  std::vector<std::vector<Float_t>> optimum_per_signal;
  for ( int signal_loop = 0; signal_loop < sig_number; signal_loop++ ) {

    /*
    cout << "sig number, signal file " << sig_number << "\t"
	 << sig_files[signal_loop] << endl;
    */
    TFile *f = new TFile(sig_files[signal_loop]);
    
    TTreeReader nomsig("Nominal",f);
    TTreeReaderValue<Float_t> wei(nomsig,"EventWeight");
    TTreeReaderValue<std::string> reg(nomsig,"m_region");

    TTreeReaderValue<Float_t> mbb(nomsig,"m_FJm");
    TTreeReaderValue<Float_t> mtautau(nomsig,gDTM);
    TTreeReaderValue<Int_t> nbtag(nomsig,"m_FJNbtagJets");


    TString sample_name;
    for ( int l = 0; l < sig_samples.size(); l++ ){
      if ( m_debug ){
	cout << "l, signal_loop, sig_files[signal_loop], sig_samples.at(l)\n"
	     << l << " " << signal_loop << " " << sig_files[signal_loop]
	     << " " << sig_samples.at(l) << endl;
      }
      if ( sig_files[signal_loop].Contains(sig_samples.at(l)) )
	sample_name = sig_samples.at(l);
    }
    cout << "sample name " << sample_name << endl;

    TString sample_name_copy = sample_name;
    Float_t mass = sample_name_copy.ReplaceAll("Xtohh","").Atof();
    
    // OUTPUT FILES
    /*
    full_output_file_filename[signal_loop]
      = out_path+"full_output_"+sample_name+"_"+gBtag+"_"+gShape+".txt";
    full_output_file[signal_loop].open(full_output_file_filename[signal_loop]);
    full_output_file[signal_loop] << "# Signal mass " << sample_name << "\n"
				  << "# Xhh\tSignif.\tMbb c.\tMtautau center\n";
    */
    
    // SIGNAL HISTOGRAMS
    TString sig_hist_title = sample_name+" "+gBtag+";m_{bb};m_{#tau#tau}";
    TH2F sig_hist (sample_name,sig_hist_title,
		   bin_n,bin_min,bin_max,bin_n,bin_min,bin_max);
    while ( nomsig.Next() ){
      TString btag = Form("%i",*nbtag);
      if ( !gBtag.Contains(btag) ){
	continue;
      }
      if ( *reg == "SR_Pre2DMassCut" || gDPhiInclusive )
	sig_hist.Fill(*mbb/1000.0,*mtautau/1000.0,*wei/gXsection);
    }
    sig_hist_vector.push_back(sig_hist);

    // MASS CENTER PER SIGNAL
    Float_t bb_center = 0;
    Float_t tt_center = 0;  
    TH1 * bb_hist = sig_hist.ProjectionX();
    // mbb center is max of X projection
    bb_center = bb_hist->GetBinCenter(bb_hist->GetMaximumBin());
    
    // for mtt center it is better to remove the "tails" in mbb
    TH1 * tt_hist = new TH1F("tt_hist","mtautau histogram",bin_n,bin_min,bin_max);
    nomsig.Restart();
    while ( nomsig.Next() ) {
      TString btag = Form("%i",*nbtag);
      if ( !gBtag.Contains(btag) ){
	continue;
      }
      if ( *mbb/1000.0 > bb_center-20.0 && *mbb/1000.0 < bb_center+20.0 ) {
	tt_hist->Fill(*mtautau/1000.0,*wei/gXsection);
      }
    }
    tt_center = tt_hist->GetMean();
    delete tt_hist;

    std::vector<Float_t> signal_and_mass_center = {mass,bb_center,tt_center};
    optimum_per_signal.push_back(signal_and_mass_center);
    cout << "SIG INTEGRAL: " << sig_hist.Integral() << endl;
    delete f;
  }
  // FINISHED READING SIG AND SAMPLE FILES
  // CENTERS SAVED ON optimum_per_signal
  //continue;

  if ( m_debug ) {
    cout << "OPTIMUM PER SIGNAL VECTOR : "  << endl;
    cout << "op size: " <<  optimum_per_signal.size() << endl;
    for ( int i = 0; i < optimum_per_signal.size(); i++ ) {
      cout << "op[i] size: " <<  optimum_per_signal.size() << endl;
      for ( int j = 0; j < optimum_per_signal[i].size(); j++ ) {
	cout << optimum_per_signal[i][j] << " ";
      }
      cout << endl;
    }
  }
  
  int nbinsx = bkg_hist->GetXaxis()->GetNbins();
  int nbinsy = bkg_hist->GetYaxis()->GetNbins();

  Float_t R, mass, significance;
  Float_t xhh_fixed, significance_fixed;

  // shape "radius" (Xhh) variation
  //cout << (gXhh_max-gXhh_min)/gXhh_step << endl;
  
  Float_t Xhh_best = 0;
  Float_t significance_best = 0;

  // OPTIMUM PER SIGNAL
  
  // here's a backup in case you want to run Optimum() a second time
  std::vector<std::vector<Float_t>> optimum_per_signal_copy = optimum_per_signal;

  // you pass optimum_per_signal as v[8][3] and returns as v[8][5] adding xhh and Z
  optimum_per_signal = Optimum(sig_hist_vector, bkg_hist,optimum_per_signal);


  // square cuts
  std::vector<std::vector<Float_t>> square_cuts = optimum_per_signal_copy;
  //square_cuts = Optimum(sig_hist_vector, bkg_hist,square_cuts,"square");
  
  // INFORMATION OF OPTIMUM VALUES FOR EACH SIGNAL ARE STORED ON optimum_per_signal
  // optimum_per_signal[j]    : different signals
  // optimum_per_signal[j][0] : mass of signal j
  // optimum_per_signal[j][1] : optimized mbb center for signal j
  // optimum_per_signal[j][2] : optimized mtt center for signal j
  // optimum_per_signal[j][3] : optimized Xhh for signal j
  // optimum_per_signal[j][4] : maximum significance Z(mbb,mtt,Xhh) obtained for signal j


  
  // OPTIMUM FIXED

  Float_t significance_best_fixed[8];
  std::vector<std::vector<Float_t>> optimum_fixed;
  optimum_fixed = Fixed(sig_hist_vector,bkg_hist,optimum_per_signal);
  Float_t Xhh_best_fixed = optimum_fixed[0][3];

  Compare_shape_and_best_vert(sig_hist_vector, optimum_per_signal, optimum_fixed,"vert");
  //Compare_shape_and_best_vert(sig_hist_vector,        square_cuts, optimum_fixed,"square");


  // SAVING TO DISK
  // PLOTS, SIGNAL OPTIMIZED
  for ( int j = 0; j < optimum_per_signal.size(); j++ ) {    
    TString sample_name = sig_hist_vector[j].GetName();
    TH2F sig_hist = sig_hist_vector[j];
    TString sample_name_copy = sample_name;
    mass = sample_name_copy.ReplaceAll("Xtohh","").Atof();

    TString sig_export = out_path+"_"+sample_name;    
    Xhh_best  = optimum_per_signal[j][3];
    Float_t bb_center = optimum_per_signal[j][1];
    Float_t tt_center = optimum_per_signal[j][2];

    TH2F * sig_hist_ptr = &sig_hist;
    
    export_hist_elip(sig_hist_ptr,sig_export,Xhh_best,bb_center,tt_center);
    bkg_export=out_path+"_bkg"+sample_name;
    export_hist_elip(bkg_hist,bkg_export,Xhh_best,bb_center,tt_center);
  }

  // PLOTS, SINGLE RESULT
  for ( int j = 0; j < sig_hist_vector.size(); j++ ) {
    TString sample_name = sig_hist_vector[j].GetName();
    TH2F sig_hist = sig_hist_vector[j];
    TString sig_export_fixed = out_path+"_fixed_"+sample_name;

    TH2F * sig_hist_ptr = &sig_hist;
    export_hist_elip(sig_hist_ptr,sig_export_fixed,Xhh_best_fixed,gbb_center_fixed,gtt_center_fixed);
    TString bkg_export_fixed=out_path+"_fixed_bkg"+sample_name;
    export_hist_elip(bkg_hist,bkg_export_fixed,Xhh_best,gbb_center_fixed,gtt_center_fixed);
  }
    
  cout << "finished correctly" << endl;
} // END MAIN


////////////////////////////
////////////////////////////
/// Functions
////////////////////////////
////////////////////////////


void draw_shape(Float_t xhh, Float_t xbb, Float_t xtt, TString shape = gShape)
{
  if ( shape == "poly" ) {
    // POLYLINE
    Float_t errbb = 0.1*xbb;
    Float_t errtt = 0.1*xtt;
    int points = 200; // polyline points, use an even number
    Float_t x[points+1];
    Float_t y[points+1];
    //Float_t xlow = xbb - xhh*errbb+4;
    //Float_t xhi  = xbb + xhh*errbb+6;
    Float_t xlow = (10.0*xbb)/(10.0+xhh);
    Float_t xhi  = (10.0*xbb)/(10.0-xhh);
    Float_t range = (xhi - xlow)/(points/2.0);
    Float_t running = xlow;
    for ( int i = 0; i < points/2; i++ ) {
      x[i] = running;
      y[i] = (10*xtt)/(10+sqrt(abs( xhh*xhh-pow((10.0-(10*xbb/x[i])),2) )));
      //ellipse:
      //y[i] = xtt - errtt * sqrt(abs(xhh*xhh-pow((xbb-running)/errbb,2)));
      running+=range;
    }
    for ( int i = points/2; i < points; i++ ) {
      x[i] = running;
      y[i] = (10*xtt)/(10-sqrt(abs( xhh*xhh-pow((10.0-(10*xbb/x[i])),2) )));
      //ellipse:
      //y[i] = xtt + errtt * sqrt(abs(xhh*xhh-pow((xbb-running)/errbb,2)));
      running-=range;
    }
    //this closes the the loop
    x[points] = x[0];
    y[points] = y[0];
    
    
    for ( int i = 0; i < 200; i++ ) {
      //cout << "i, x,y " << i << " " << x[i] << " " << y[i] << endl;
    }
    TPolyLine * polyline =  new TPolyLine(201,x,y);
    polyline->SetLineColor(2);
    polyline->SetLineWidth(4);
    polyline->Draw();
    return;
  } else if ( shape == "elip" ) {
    TEllipse * elip  = new TEllipse(xbb,xtt,xhh*0.1*xbb,xhh*0.1*xtt);
    elip->SetFillColor(0);
    elip->SetFillStyle(0);
    elip->SetLineColor(2);
    elip->SetLineWidth(4);
    elip->Draw();
    return;
  } else if ( shape == "vert" ) {
    TEllipse * elip  = new TEllipse(xbb,xtt,xhh*gVertbb*xbb,xhh*gVerttt*xtt);
    elip->SetFillColor(0);
    elip->SetFillStyle(0);
    elip->SetLineColor(2);
    elip->SetLineWidth(4);
    elip->Draw();
    return;
  } else if ( shape == "square" ) {
    Float_t x[5] = {gSquare_bb_min, gSquare_bb_max, gSquare_bb_max, gSquare_bb_min, gSquare_bb_min};
    Float_t y[5] = {gSquare_tt_min, gSquare_tt_min, gSquare_tt_max, gSquare_tt_max, gSquare_tt_min};
    TPolyLine * polyline =  new TPolyLine(5,x,y);
    polyline->SetLineColor(2);
    polyline->SetLineWidth(4);
    polyline->Draw();
    //    TBox * mybox = new TBox(gSquare_bb_min,gSquare_tt_min,gSquare_bb_max,gSquare_tt_max);
    /*
    mybox->SetFillColor(0);
    mybox->SetFillStyle(0);
    mybox->SetLineColor(2);
    mybox->SetLineWidth(4);*/
    //mybox->Draw();
    return;
  }
}


// exports histograms
// must include the full path in the output filename
void export_hist(TH2 * h2,
		 TString out_filename,
		 TString options = "colz") {
  auto c1 = new TCanvas();
  c1->SetCanvasSize(1500,1500);
  c1->SetLeftMargin(0.12);
  c1->SetRightMargin(0.15);
  h2->Draw(options);

  
  c1->SaveAs(out_filename);
  c1->Modified();
  c1->Update();
  delete c1;
}

void export_hist_elip(TH2 * h2,
		      TString out_filename,
		      Float_t xhh,
		      Float_t xbb,
		      Float_t xtt,
		      TString shape = gShape
		      ) {
  auto c1 = new TCanvas();
  TString options = "colz";
  c1->SetCanvasSize(1500,1500);
  c1->SetLeftMargin(0.12);
  c1->SetRightMargin(0.15);
  h2->Draw(options);

  // draw center
  if ( shape == "vert" || shape == "poly" || shape == "elip" ) {
    TEllipse * elip  = new TEllipse(xbb,xtt,1.,1.);
    elip->SetFillColor(0);
    elip->SetFillStyle(0);
    elip->SetLineColor(2);
    elip->SetLineWidth(4);
    elip->Draw();
  }
  if ( gShape != "none" ) { 
    draw_shape(xhh,xbb,xtt);
  }
  out_filename+="_"+gBtag;
  out_filename+="_dPhi";
  if ( gDPhiInclusive ) {
    out_filename +="Inclusive";
  } else if ( !gDPhiInclusive ) {
    out_filename +="Cut";
  }
  if ( gMultipleWindows ) {
    out_filename += "_"+gMassWindow;
  } else if ( !gMultipleWindows ) {
    out_filename += "_single";
  }
  out_filename+="_"+shape;
  out_filename+=".png"; // don't put the extension until the very end!
  c1->SaveAs(out_filename);
  c1->Modified();
  c1->Update();
  delete c1;
}


// This function appends xhh and Z to a vector already containing mass, mbbcenter and mttcenter
// it does so on a per-signal basis
std::vector<std::vector<Float_t>> Optimum(
					   std::vector<TH2F>sig_hist_vector,
					   TH2 * bkg_hist,
					   std::vector<std::vector<Float_t>> optimum_per_signal,
					   TString shape = gShape
					   )
{
  cout << "\nOPTIMUM PER SIGNAL FOR " << shape << " SHAPE" << endl;
  cout << "|signal mass | Xhh | Z | Mbb center | Mtt center|" << endl;
  cout << "|------------+-----+---+------------+-----------|" << endl;

  ofstream optimum_file;
  TString out_optimum_file;
  out_optimum_file = out_path+"optimum_"+gBtag+"_dPhi";
  if ( gDPhiInclusive ) {
    out_optimum_file += "Inclusive_";
  } else if ( !gDPhiInclusive ) {
    out_optimum_file += "Cut_";
  }
  if ( gMultipleWindows ) {
    out_optimum_file += gMassWindow+"_"; // either high or low
  } else if ( !gMultipleWindows ) {
    out_optimum_file += "single_";
  }
  out_optimum_file += gShape;
  out_optimum_file += ".txt";
  optimum_file.open(out_optimum_file);
  //optimum_file << "# XhhM\tXhh\tZ.\tMbb c\tMtautau c\n";

  optimum_file << "#OPTIMUM PER SIGNAL FOR " << shape << " SHAPE" << endl;
  optimum_file << "#signal mass | Xhh | Z | Mbb center | Mtt center|" << endl;
  
  int nbinsx = bkg_hist->GetXaxis()->GetNbins();
  int nbinsy = bkg_hist->GetYaxis()->GetNbins();
  
  for ( int i = 0; i < sig_hist_vector.size(); i++ ) {
    TString sample_name = sig_hist_vector[i].GetName();
    TString sample_name_copy = sample_name;
    Float_t mass = sample_name_copy.ReplaceAll("Xtohh","").Atof();
    TH2F sig_hist = sig_hist_vector[i];

    Float_t Xhh_best = 0;
    Float_t significance_best = 0;

    Float_t bb_center,tt_center;
    for ( int j = 0; j < optimum_per_signal.size(); j++ ) {
      if ( mass == optimum_per_signal[j][0] ) {
	bb_center = optimum_per_signal[j][1];
	tt_center = optimum_per_signal[j][2];
      }
    }

    
    for ( Float_t R = gXhh_min; R <= gXhh_max; R+= gXhh_step ) {
      Float_t integrated_sig = 0;
      Float_t integrated_bkg = 0;
      for ( int x = 0; x < nbinsx; x++ ) {
	for ( int y = 0; y < nbinsy; y++ ) {
	  Float_t bin_center_x = ((TAxis*)bkg_hist->GetXaxis())->GetBinCenter(x);
	  Float_t bin_center_y = ((TAxis*)bkg_hist->GetYaxis())->GetBinCenter(y);
	  Float_t xhh = Xhh(bb_center,tt_center,bin_center_x,bin_center_y, shape);
	  // signal and background within shape of radius R
	  if ( xhh < R  ) {
	    integrated_sig += sig_hist.GetBinContent(x,y);
	    integrated_bkg += bkg_hist->GetBinContent(x,y);
	  }
	}
      }
      Float_t significance = 0;
      significance = Significance(integrated_sig,integrated_bkg);

      if ( significance > significance_best ) {	
	significance_best = significance;
	Xhh_best = R;
      }
    }
    for ( int j = 0; j < optimum_per_signal.size(); j++ ) {
      if ( mass == optimum_per_signal[j][0] ) {
	optimum_per_signal[j].push_back(Xhh_best);
	optimum_per_signal[j].push_back(significance_best);
      }
    }
    cout << Form("| %.0f | %.2f | %.3f | %.2f | %.2f |",
		 mass,Xhh_best,significance_best,bb_center,tt_center) << endl;
    optimum_file << Form("%.0f\t%.2f\t%.3f\t%.2f\t%.2f",
			 mass,Xhh_best,significance_best,bb_center,tt_center) << endl;
  }
  cout << endl;
  optimum_file.close();
  return optimum_per_signal;
}




std::vector<std::vector<Float_t>> Fixed(
					std::vector<TH2F>sig_hist_vector,
					TH2 * bkg_hist,
					std::vector<std::vector<Float_t>> optimum_per_signal,
					TString shape = gShape
					)
{
  std::vector<std::vector<Float_t>> optimum_fixed;
  std::vector<Float_t> significance_best_fixed;
  std::vector<Float_t> significance_fixed;

  Float_t deviation_best = 1000;
  Float_t Xhh_best_fixed;
  
  int nbinsx = bkg_hist->GetXaxis()->GetNbins();
  int nbinsy = bkg_hist->GetYaxis()->GetNbins();
  
  for ( int i = 0; i < sig_hist_vector.size(); i++ ) {
    TString sample_name = sig_hist_vector[i].GetName();
    TString sample_name_copy = sample_name;
    Float_t mass = sample_name_copy.ReplaceAll("Xtohh","").Atof();
    std::vector<Float_t> signal_vector = { mass, gbb_center_fixed, gtt_center_fixed };
    optimum_fixed.push_back(signal_vector);
    significance_best_fixed.push_back(0);
  }

  // Loop for different values of Xhh
  for ( Float_t R = gXhh_min; R <= gXhh_max; R+= gXhh_step ) {

    Float_t deviation = 0;

    significance_fixed.clear();
    for ( int i = 0; i < sig_hist_vector.size(); i++ ) {
      TString sample_name = sig_hist_vector[i].GetName();
      TString sample_name_copy = sample_name;
      Float_t mass = sample_name_copy.ReplaceAll("Xtohh","").Atof();
      TH2F sig_hist = sig_hist_vector[i];
      
      // calculate significance for each shape
      Float_t integrated_sig_fixed = 0;
      Float_t integrated_bkg_fixed = 0;
      for ( int x = 0; x < nbinsx; x++ ) {
	for ( int y = 0; y < nbinsy; y++ ) {
	  Float_t bin_center_x = ((TAxis*)bkg_hist->GetXaxis())->GetBinCenter(x);
	  Float_t bin_center_y = ((TAxis*)bkg_hist->GetYaxis())->GetBinCenter(y);

	  Float_t xhh_fixed = Xhh(gbb_center_fixed,gtt_center_fixed,bin_center_x,bin_center_y, shape);
	  // signal and background within shape of radius R
	  if ( xhh_fixed < R ) {
	    integrated_sig_fixed += sig_hist.GetBinContent(x,y);
	    integrated_bkg_fixed += bkg_hist->GetBinContent(x,y);
	  }
	}
      }
      // please don't divide by zero
      /*
      if ( integrated_bkg == 0 )
	continue;
      */
      
      // SIGNIFICANCE (Z)

      significance_fixed.push_back(Significance(integrated_sig_fixed, integrated_bkg_fixed));

      /*
      full_output_file[signal_loop] <<
	Form("%.2f\t%.3f\t%.2f\t%.2f",R,significance,bb_center,tt_center) << endl;
      */
      deviation += pow( optimum_per_signal[i][4]-significance_fixed[i],2);
    }

    // check if deviation from optimum values is better (smaller) for
    // this R
    if ( deviation < deviation_best ) {
      deviation_best   = deviation;
      for ( int j = 0; j < sig_hist_vector.size(); j++ ) {
	significance_best_fixed[j] = significance_fixed[j];
      }
      Xhh_best_fixed = R;
    }
  }
  for ( int i = 0; i < sig_hist_vector.size(); i++ ) {
    optimum_fixed[i].push_back(Xhh_best_fixed);
    optimum_fixed[i].push_back(significance_best_fixed[i]);
  }

  return optimum_fixed;
}

void Compare_shape_and_best_vert(
				 std::vector<TH2F> sig_hist_vector,
				 std::vector<std::vector<Float_t>> optimum_per_signal,
				 std::vector<std::vector<Float_t>> optimum_fixed,
				 TString shape = gShape
				 )
{
  ofstream optimum_file;
  TString out_optimum_file;
  out_optimum_file = out_path+"fixed_"+gBtag+"_dPhi";
  if ( gDPhiInclusive ) {
    out_optimum_file += "Inclusive_";
  } else if ( !gDPhiInclusive ) {
    out_optimum_file += "Cut_";
  }
  if ( gMultipleWindows ) {
    out_optimum_file += gMassWindow+"_"; // either high or low
  } else if ( !gMultipleWindows ) {
    out_optimum_file += "single_";
  }
  out_optimum_file += gShape;
  out_optimum_file += ".txt";
  optimum_file.open(out_optimum_file);
  //optimum_file << "# XhhM\tXhh\tZ.\tMbb c\tMtautau c\n";

  optimum_file << "#Comparing OPTIMUM FIXED with " << shape << " mass window" << endl;
  optimum_file << "#X_{HH}^{\\text{fixed}} = " << optimum_fixed[0][3] << endl;
  optimum_file << "#(m_{bb}^{\\text{center}}, m_{\\tau\\tau}^{\\text{center}}) = "
	       << "(" << optimum_fixed[0][1] << ", " << optimum_fixed[0][2] << ")" << endl;

  optimum_file << "#signal mass | Z (signal optimized) | Z (fixed) | Reduction (%)" << endl;
  
  
      
  Float_t deviation_best = 1000;
  
  // COUT OPTIMUM FIXED
  cout << "\nComparing OPTIMUM FIXED with " << shape << " mass window" << endl;
  //cout << "X_{HH} = " << Xhh_best_fixed << endl;
  cout << "X_{HH}^{\\text{fixed}} = " << optimum_fixed[0][3] << endl;
  cout << "(m_{bb}^{\\text{center}}, m_{\\tau\\tau}^{\\text{center}}) = "
       << "(" << optimum_fixed[0][1] << ", " << optimum_fixed[0][2] << ")" << endl;
  cout << "|signal mass | Z (signal optimized) | Z (fixed) | Reduction (%) |" << endl;
  cout << "|------------+----------------------+-----------+---------------|" << endl;
  for ( int j = 0; j < sig_hist_vector.size(); j++ ) {
    TString sample_name = sig_hist_vector[j].GetName();
    TH2F sig_hist = sig_hist_vector[j];
    TString sample_name_copy = sample_name;
      Float_t mass = sample_name_copy.ReplaceAll("Xtohh","").Atof();
      
      //Float_t reduction = (1-significance_best_fixed[j]/optimum_per_signal[j][4])*100;
      Float_t reduction = (1-optimum_fixed[j][4]/optimum_per_signal[j][4])*100;
      cout << Form("| %.0f | %.3f | %.3f | %06.3f |",
		   mass,optimum_per_signal[j][4],optimum_fixed[j][4],reduction) << endl;
      optimum_file << Form("%.0f\t%.3f\t%.3f\t%06.3f",
			   mass,optimum_per_signal[j][4],optimum_fixed[j][4],reduction) << endl;
  }
  cout << endl;
  optimum_file.close();
}
