#include "TSideband.cxx"

void Draw_th1(TH1F *, TH1F *, TString = "");
void Draw_th1(TH1D *, TH1D *, TString = "");
void Draw_th2(TH2F *, TH2F *, TString = "");
TString gOut_path = "/home/luciano/Physics/ATLAS/Sideband_plots/toy_data/";

void toy_data()
{
  TFile *dat_file = new TFile("/home/luciano/Physics/ATLAS/data/MVA_1201/data.root");
  TFile *sig_file = new TFile("/home/luciano/Physics/ATLAS/data/MVA_1201/Xtohh2000_Herwig.root");
  TFile *bkg_file = new TFile("/home/luciano/Physics/ATLAS/data/MVA_1201/mc.root");

  // generates all histograms
  // TSideband ( TFile *f, TString filetype, TString region_cut, int btag_cut, int antibtag, int antittag)
  //TSideband * a = new TSideband(dat_file,"data", "SR", 2);
  TSideband * b = new TSideband(sig_file,"sig", "SR",2);
  TSideband * c = new TSideband(bkg_file,"bkg", "SR",2);
  //a->FillHistos();

  /*
  TH1F *h = (TH1F*) gDirectory->Get("SB_sig")->ProyectionX();
  TH1F *h2 = (TH1F*) gDirectory->Get("SB_bkg")->ProyectionY();

  Draw_th1(h,h2, gOut_path+"sig_bkg.pdf");
  
  cout << "|     | Entries | Integral|" << endl;
  cout << "| Sig | " << h->GetEntries()  << " | "  << h->Integral() << " |" << endl;
  cout << "| Bkg | " << h2->GetEntries()  << " | "  << h2->Integral() << " |" << endl;
  cout << "|-----+---------+---------|" << endl;
  */
 
  
  TH2F *h2d = (TH2F*) gDirectory->Get("SB_sig");
  TH2F *h2d2 = (TH2F*) gDirectory->Get("SB_bkg");

  TH2F *h2d5 = (TH2F*) gDirectory->Get("CR_sig");
  TH2F *h2d6 = (TH2F*) gDirectory->Get("CR_bkg");

  Draw_th1(h2d->ProjectionX(),h2d2->ProjectionX(),gOut_path+"SB_toy_data_anti2btag.pdf");
  cout << "|     | Entries | Integral|" << endl;
  cout << "| Sig | " << h2d->GetEntries()  << " | "  << h2d->Integral() << " |" << endl;
  cout << "| Bkg | " << h2d2->GetEntries()  << " | "  << h2d2->Integral() << " |" << endl;
  cout << "|-----+---------+---------|" << endl;

  Draw_th1(h2d5->ProjectionX(),h2d6->ProjectionX(),gOut_path+"CR_toy_data_anti2btag.pdf");
  cout << "|     | Entries | Integral|" << endl;
  cout << "| Sig | " << h2d5->GetEntries()  << " | "  << h2d5->Integral() << " |" << endl;
  cout << "| Bkg | " << h2d6->GetEntries()  << " | "  << h2d6->Integral() << " |" << endl;
  cout << "|-----+---------+---------|" << endl;

  
  /*
  Draw_th2(h2d, h2d2,gOut_path+"2d_SB.pdf");

  TH2F *h2d3 = (TH2F*) gDirectory->Get("SR_sig");
  TH2F *h2d4 = (TH2F*) gDirectory->Get("SR_bkg");
  Draw_th2(h2d3, h2d4,gOut_path+"2d_SR.pdf");

  TH2F *h2d5 = (TH2F*) gDirectory->Get("CR_sig");
  TH2F *h2d6 = (TH2F*) gDirectory->Get("CR_bkg");
  Draw_th2(h2d5, h2d6,gOut_path+"2d_CR.pdf");
  */
}

void Draw_th1(TH1F * sig, TH1F * bkg, TString out_filename = "")
{
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  TH1 * sum = new TH1F("sum","Toy data",28,40,180);
  TCanvas * c1 = new TCanvas();
  c1->SetCanvasSize(1500,1500);
  //c1->SetLeftMargin(0.12);
  //c1->SetRightMargin(0.15);

  sum->Add(sig);
  sum->Add(bkg);
  
  bkg->SetFillColor(kBlue);
  bkg->SetFillStyle(3353);

  sig->SetLineColor(kRed);
  sig->SetFillColor(kRed);
  sig->SetFillStyle(3335);

  sum->SetLineColor(kBlack);
  
  bkg->SetAxisRange(50,180);
  sig->SetAxisRange(50,180);
  sum->SetAxisRange(50,180);

  bkg->SetTitle("");
  bkg->Draw("HIST");  
  sig->Draw("HIST same");

  sum->Draw("same");

  
  bkg->SetTitle("Weighted background");
  sig->SetTitle("Weighted signal");
  TLegend *myleg;
  myleg = c1->BuildLegend(0.4,0.6,0.9,0.89,"","");
  myleg->SetFillStyle(0);

  if ( out_filename != "" ) {
    c1->SaveAs(out_filename);
  }
}

void Draw_th1(TH1D * sig, TH1D * bkg, TString out_filename = "")
{
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  TH1 * sum = new TH1D("sum","Toy data",28,40,180);
  TCanvas * c1 = new TCanvas();
  c1->SetCanvasSize(500,500);
  //c1->SetLeftMargin(0.12);
  //c1->SetRightMargin(0.15);

  sum->Add(sig);
  sum->Add(bkg);
  
  bkg->SetFillColor(kBlue);
  bkg->SetFillStyle(3353);

  sig->SetLineColor(kRed);
  sig->SetFillColor(kRed);
  sig->SetFillStyle(3335);

  sum->SetLineColor(kBlack);
  
  bkg->SetAxisRange(50,180);
  sig->SetAxisRange(50,180);
  sum->SetAxisRange(50,180);

  bkg->SetTitle("");
  bkg->Draw("HIST");  
  sig->Draw("HIST same");

  sum->Draw("same");

  
  bkg->SetTitle("Weighted background");
  sig->SetTitle("Weighted signal");
  TLegend *myleg;
  myleg = c1->BuildLegend(0.4,0.6,0.9,0.89,"","");
  myleg->SetFillStyle(0);

  if ( out_filename != "" ) {
    c1->SaveAs(out_filename);
  }
}

void Draw_th2(TH2F * sig, TH2F * bkg, TString out_filename = "")
{
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  TH2 * sum = new TH2F("sum","Toy data",80,0,400, 80,0,400);
  TCanvas * c1 = new TCanvas();
  c1->SetCanvasSize(500,500);
  //c1->SetLeftMargin(0.12);
  //c1->SetRightMargin(0.15);

  sum->Add(sig);
  sum->Add(bkg);

  /*
  bkg->SetFillColor(kBlue);
  bkg->SetFillStyle(3353);

  sig->SetLineColor(kRed);
  sig->SetFillColor(kRed);
  sig->SetFillStyle(3335);

  sum->SetLineColor(kBlack);
  
  bkg->SetAxisRange(50,180);
  sig->SetAxisRange(50,180);
  sum->SetAxisRange(50,180);

  bkg->SetTitle("");
  bkg->Draw("colz");  
  sig->Draw("HIST same");
  */
  sum->Draw("colz");

  /*  
  bkg->SetTitle("Weighted background");
  sig->SetTitle("Weighted signal");
  TLegend *myleg;
  myleg = c1->BuildLegend(0.4,0.6,0.9,0.89,"","");
  myleg->SetFillStyle(0);
  */
  if ( out_filename != "" ) {
    c1->SaveAs(out_filename);
  }
}
