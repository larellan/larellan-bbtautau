class TSideband
{
private:
  TH2F fSB;
  TH2F fSR;
  TH2F fCR;
  TH2F fFull;
  TH1F fbb;
  TH1F ftt;
  TString fRegion_cut;
  Int_t fBtag_cut;
  Int_t fAntibtag_cut;
  Int_t fAntittag_cut;
  TFile * fFile;
public:
  TSideband(TFile *, TString = "", TString = "", Int_t = -1, Int_t = -1, Int_t = -1);
  void FillHistos(TFile *, TString ="", TString = "", Int_t = -1, Int_t = -1, Int_t = -1);
  void ClearHistos();
  TH2F GetSB() { return fSB; }
  TH2F GetSR() { return fSR; }
  TH2F GetCR() { return fCR; }
  TH1F Getbb() { return fbb; }
  TH1F Gettt() { return ftt; }
};

bool Xhh(double mbb, double mtt){
  if ( sqrt( pow((122.5-mbb)/(0.1*122.5*2.06),2) +  pow((81.0-mtt)/(0.25*81.0*2.06),2) ) < 1.0 )
    return 1;
  else return 0;
}

bool CRfat(double mbb, double mtt){
  if ( sqrt( pow((122.5-mbb)/(0.15*122.5*2.06),2) +  pow((81.0-mtt)/(0.3*81.0*2.06),2) ) < 1.0 )
    return 1;
  else return 0;
}

bool SBfat(double mbb, double mtt){
  if ( sqrt( pow((135.0-mbb)/(0.25*122.5*2.06),2) +  pow((81.0-mtt)/(0.35*81.0*2.06),2) )  < 1.0 )
    return 1;
  else return 0;
}

bool PassCuts(Float_t cut1,
	      Float_t cut2,
	      Float_t cut3,
	      Float_t cut4,
	      Float_t cut5,
	      Float_t cut6,
	      Float_t cut7,
	      Float_t cut8,
	      Float_t cut9,
	      Float_t cuteta)
{
  bool answer = true;
  if ( cut1 > 3 ) { // N subjets
    answer = false;
    return answer;
  }
  /*
  if ( cut2 < 0.52 ) { // BDT score
    answer = false;
    return answer;
  }
  */
  if ( cut3 > 0.8 ) { // DeltaR (lead, subl)
    answer = false;
    return answer;
  }
  if ( cut4 != 1 && cut4 != 3 ) { // lead tracks
    answer = false;
    return answer;
  }
  if ( cut5 != 1 && cut5 != 3) { // subl tracks
    answer = false;
    return answer;
  }
  if ( cut6 != 1 && cut6 != -1 ) { // lead charge
    answer = false;
    return answer;
  }
  if ( cut7 != 1 && cut7 != -1 ) { // subl charge
    answer = false;
    return answer;
  }
  if ( cut8 < 50) { // sublead pt
    //answer = false;
    return answer;
  }
  if ( cut9 < 300 ) { // substructure pt
    //answer = false;
    return answer;
  }
  if ( ( fabs(cuteta) >= 1.37 && fabs(cuteta) < 1.52 )|| fabs(cuteta) > 2.0 ) {
    answer = false;
    return answer;
  }
    
  return answer;
}


TSideband::TSideband(
		     TFile * f,
		     TString file_type = "" ,
		     TString region_cut = "",
		     Int_t btag_cut = -1,
		     Int_t antibtag = -1,
		     Int_t antittag = -1 )
{
  fFile = f;
  fRegion_cut = region_cut;
  fBtag_cut = btag_cut;
  FillHistos(f, file_type,region_cut,btag_cut,antibtag, antittag);
}


void TSideband::FillHistos(
			   TFile * f,
			   TString file_type = "" ,
			   TString region_cut = "",
			   Int_t btag_cut = -1,
			   Int_t antibtag = -1,
			   Int_t antittag = -1  )
{
  
  TTreeReader nom("Nominal",f);
  
  TTreeReaderValue<Float_t> weight(nom,"EventWeight");
  TTreeReaderValue<std::string> reg(nom,"m_region");
  TTreeReaderValue<std::string> sample(nom,"sample");
  TTreeReaderValue<Float_t> mbb(nom,"m_FJm");
  TTreeReaderValue<Float_t> mtt(nom,"m_DTm");
  TTreeReaderValue<Int_t> nbtag(nom,"m_FJNbtagJets");
  TTreeReaderValue<Int_t> abtag(nom,"m_AntiBTag");
  TTreeReaderValue<Int_t> attag(nom,"m_AntiTauTag");

  TTreeReaderValue<ULong64_t> eventnumber(nom,"EventNumber");


  // DT cuts
  //TTreeReaderValue<Float_t> cut0(nom,"m_DTm");
  
  TTreeReaderValue<Float_t> cut1_reader(nom,"m_DTsubjets");
  TTreeReaderValue<Float_t> cut2_reader(nom,"m_DTBDTscore");
  TTreeReaderValue<Float_t> cut3_reader(nom,"m_DTdRleadsub");
  TTreeReaderValue<Float_t> cut4_reader(nom,"m_DTLeadNTracks");
  TTreeReaderValue<Float_t> cut5_reader(nom,"m_DTSublNTracks");
  TTreeReaderValue<Float_t> cut6_reader(nom,"m_DTLeadCharge");
  TTreeReaderValue<Float_t> cut7_reader(nom,"m_DTSublCharge");
  TTreeReaderValue<Float_t> cut8_reader(nom,"m_DTSublpT");
  TTreeReaderValue<Float_t> cut9_reader(nom,"m_DTSubstructurepT");
  TTreeReaderValue<Float_t> dteta(nom,"m_DTeta");
  
  TH2F th2_signal("SR_"+file_type,
		  "Signal region "+file_type+";m_{bb} [GeV];m_{#tau#tau} [GeV]",
		  60,0,300, 60, 0 ,300);
  TH2F th2_sideband("SB_"+file_type,
		    "Sideband "+file_type+";m_{bb} [GeV];m_{#tau#tau} [GeV]",
		    60,0,300, 60, 0 ,300);
  TH2F th2_validation("CR_"+file_type,
		      "Validation "+file_type+"region;m_{bb} [GeV];m_{#tau#tau} [GeV]",
		      60,0,300, 60, 0 ,300);

  TH2F th2_full("Full_"+file_type,
		"2D full region;m_{bb} [GeV];m_{#tau#tau} [GeV]",
		60,0,300, 60, 0 ,300);
  
  TH1F th1_full_bb("bb_"+file_type,
		   ";m_{bb} [GeV]; Events/5 GeV",
		   40,0,200);
  
  TH1F th1_full_tt("tt_"+file_type,
		   ";m_{#tau#tau} [GeV]; Events/5 GeV",
		   40,0,200);
  
  Float_t local_weight;
  TObjArray *tx = region_cut.Tokenize(":");
  
  // File reading
  while ( nom.Next() ) {

    
    bool passed  = PassCuts(*cut1_reader,
			    *cut2_reader,
			    *cut3_reader,
			    *cut4_reader,
			    *cut5_reader,
			    *cut6_reader,
			    *cut7_reader,
			    *cut8_reader,
			    *cut9_reader,
			    *dteta
			    );
    if ( !passed )
      continue;
    
    
    local_weight = *weight;
    //local_weight = 1;
    if ( file_type == "data" ) {
      local_weight = 1; 
    }
    
    // region cut (positive cut, e.g. region_cut == SR only lets SR in)
    bool allowed_region = false;
    if ( region_cut != "" ) {
      for ( Int_t i = 0; i < tx->GetEntries(); i++ ) {
	if ( *reg == ((TObjString *)(tx->At(i)))->String() ) {
	  allowed_region = true;
	  break;
	}
	
      }
      if ( !allowed_region ) {
	continue;
      }
    }

    // btag cut
    // for inclusive 1+2 use 12
    if ( btag_cut != -1 ) {
      if ( btag_cut != 12 && *nbtag != btag_cut ) {
	continue;
      }
      if ( btag_cut == 12 && ( *nbtag != 1 && *nbtag != 2 ) ) {
	continue;
      }	
    }

    // antibtag cut
    // -1: inclusive (default)
    //  n: more than n trackjets
    
    // uncomment the following if for example in 1b2t
    // you want to ONLY allow FJ with at least 2 trackjets
    // i.e. demanding 2 trackjets even for 1btag
    /*
    if ( antibtag != -1 ) {
      if ( *abtag < antibtag ) {
	continue;
      }
    }
    */
    
    // antitau cut
    // -1: inclusive (default)
    //  0: ditaus
    //  2: fakes (aka antitau)
    if ( antittag != -1 ) {
      //if ( *attag != antittag ) {
      
      if ( (*cut2_reader > 0.60 && antittag == 2)
      || (*cut2_reader < 0.60 && antittag == 0) ) {
	continue;
      }
      
    }

   
    bool is_sr = Xhh( *mbb/1000.0, *mtt/1000.0 );
    bool is_cr = CRfat( *mbb/1000.0, *mtt/1000.0 );
    bool is_sb = SBfat( *mbb/1000.0, *mtt/1000.0 );


    // TH2 Fill
    if ( is_sr ) {
      th2_signal.Fill(*mbb/1000.0, *mtt/1000.0, local_weight);
    } else if ( is_cr && !is_sr ) {
      th2_validation.Fill(*mbb/1000.0, *mtt/1000.0, local_weight);
    } else if ( is_sb && !is_sr && !is_cr ) {
      //cout << "event passed: " << *eventnumber << endl;
      th2_sideband.Fill(*mbb/1000.0, *mtt/1000.0, local_weight);
    } 
    th2_full.Fill(*mbb/1000.0, *mtt/1000.0, local_weight);
    // TH1 Fill

    th1_full_bb.Fill(*mbb/1000.0, local_weight);
    th1_full_tt.Fill(*mtt/1000.0, local_weight);
    
  }

  fSB = th2_sideband;
  fSR = th2_signal;
  fCR = th2_validation;
  fFull = th2_full;
  fbb = th1_full_bb;
  ftt = th1_full_tt;
}

void TSideband::ClearHistos()
{
  fSB.Reset();
  fSR.Reset();
  fCR.Reset();
  fFull.Reset();
  fbb.Reset();
  ftt.Reset();
}
