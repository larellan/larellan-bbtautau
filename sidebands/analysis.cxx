#include "TSideband.cxx"
//#include "TFitSideband.cxx"
const TString fit_projection = "bb";

TH1 * data;
TH1 * bkg1;
TH1 * bkg2;


void Draw_th1(TH1F *, TH1F *, TString = "");
void Draw_th1(TH1D *, TH1D *, TString = "");
void Draw_th2(TH2F *, TString = "");

TString gOut_path = "/home/luciano/Physics/ATLAS/Sideband_plots/analysis/";

Double_t ftotal1( Double_t *x, Double_t *par ) {
   Double_t xx = x[0];
   Int_t bin = bkg1->GetXaxis()->FindBin(xx);
   Double_t br = par[0]*bkg1->GetBinContent(bin);

   return br;
}

Double_t ftotal2( Double_t *x, Double_t *par ) {
   Double_t xx = x[0];
   Int_t bin = bkg1->GetXaxis()->FindBin(xx);
   Double_t br = par[0]*bkg1->GetBinContent(bin);

   Int_t bin2 = bkg2->GetXaxis()->FindBin(xx);
   Double_t br2 = par[1]*bkg2->GetBinContent(bin2);
   
   return br + br2;
}

void analysis()
{
  std::vector<TString> sideband_regions =
    {
     "SB",
     "CR",
     "SR" // do not uncomment, blinding is your friend!
    };
  
  std::vector<TString> analysis_type =
    {
     "dat",
     //"bkg",
     //"sig",
    };
  
  std::vector<TString> matrix_region =
    {
     //"reader",
     "2b2t",
     "2b0t",
     "1b2t",
     "1b0t",
     "0b2t",
     //"0b0t",
    };
  
  TFile *dat_file = new TFile("/home/luciano/Physics/ATLAS/data/MVA_1222_cutflow/data.root");
  //TFile *dat_file = new TFile("/home/luciano/Physics/ATLAS/data/MVA_1217/data.root");
  TFile *sig_file = new TFile("/home/luciano/Physics/ATLAS/data/MVA_1222_cutflow/Xtohh2000.root");
  TFile *bkg_file = new TFile("/home/luciano/Physics/ATLAS/data/MVA_1222_cutflow/full_bkg_mc.root");

  // TSideband ( TFile *f, TString filetype, TString region_cut, int btag_cut, int antibtag, int antittag)
  //TSideband * b = new TSideband(sig_file,"sig", "SR",2);
  cout << "---------------------------------------\nREADER\n----------------------------" << endl;
  TSideband * reader = new TSideband(dat_file,"dat_reader", "SR",2,2,-1);
  cout << "---------------------------------------\n2b2t\n----------------------------" << endl;

  TSideband * bt = new TSideband(dat_file,"dat_2b2t", "SR:QCDCR",2,2,0);
  TSideband * at = new TSideband(dat_file,"dat_0b2t", "SR:QCDCR",0,2,0);
  TSideband * ba = new TSideband(dat_file,"dat_2b0t", "SR:QCDCR",2,2,2);
  TSideband * aa = new TSideband(dat_file,"dat_0b0t", "SR:QCDCR",0,2,2);
  // 1tag
  TSideband * b1t = new TSideband(dat_file,"dat_1b2t", "SR:QCDCR",1,2,0);
  TSideband * b1a = new TSideband(dat_file,"dat_1b0t", "SR:QCDCR",1,2,2);

  /*
  // MC background
  TSideband * bt_bkg = new TSideband(bkg_file,"bkg_2b2t", "SR:QCDCR",2,2,0);
  TSideband * at_bkg = new TSideband(bkg_file,"bkg_0b2t", "SR:QCDCR",0,2,0);
  TSideband * ba_bkg = new TSideband(bkg_file,"bkg_2b0t", "SR:QCDCR",2,2,2);
  TSideband * aa_bkg = new TSideband(bkg_file,"bkg_0b0t", "SR:QCDCR",0,2,2);
  // 1tag
  TSideband * b1t_bkg = new TSideband(bkg_file,"bkg_1b2t", "SR:QCDCR",1,2,0);
  TSideband * b1a_bkg = new TSideband(bkg_file,"bkg_1b0t", "SR:QCDCR",1,2,2);

  // MC signal
  TSideband * bt_sig = new TSideband(sig_file,"sig_2b2t", "SR:QCDCR",2,2,0);
  TSideband * at_sig = new TSideband(sig_file,"sig_0b2t", "SR:QCDCR",0,2,0);
  TSideband * ba_sig = new TSideband(sig_file,"sig_2b0t", "SR:QCDCR",2,2,2);
  TSideband * aa_sig = new TSideband(sig_file,"sig_0b0t", "SR:QCDCR",0,2,2);
  // 1tag
  TSideband * b1t_sig = new TSideband(sig_file,"sig_1b2t", "SR:QCDCR",1,2,0);
  TSideband * b1a_sig = new TSideband(sig_file,"sig_1b0t", "SR:QCDCR",1,2,2);
  */

  
  TString histname = "";
  //TH2F *h2;
  for ( auto reg: sideband_regions ) {
    for ( auto type: analysis_type ) {
      for ( auto mat: matrix_region ) {
	//TH2F *h2 = (TH2F*) gDirectory->Get("SB");
	histname = reg+"_"+type+"_"+mat;
	if ( histname != "" ) {
	  TH2F *h2 = (TH2F*) gDirectory->Get(histname);
	  cout << histname << "|"  << h2->GetEntries() << endl;
	  //Draw_th2(h2,gOut_path+histname+".png");
	}
      }
    }
  }

  /*
  cout << "|    | ";
  for ( auto mat: matrix_region ) {
    cout << mat << " | ";
  }
  cout << endl;
  */
  
  for ( auto type: analysis_type ) {
    cout << "Analysis type " << type << endl;
    cout << "|    | ";
    for ( auto mat: matrix_region ) {
      cout << mat << " | ";
    }
    cout << endl;
    for ( auto reg: sideband_regions ) {
      cout << "| " << reg << " | ";
      for ( auto mat: matrix_region ) {
	//TH2F *h2 = (TH2F*) gDirectory->Get("SB");
	histname = reg+"_"+type+"_"+mat;
	TH2F *h2 = (TH2F*) gDirectory->Get(histname);
	if ( reg == "SR" && mat == "2b2t" && type == "dat" ) {
	  cout << "blinded | ";
	} else {
	  cout << h2->Integral() << " | ";
	}
      }
      cout << endl;
    }
    cout << endl;
  }
  
  
  ////////////////////////////////////
  // FITTING
  ////////////////////////////////////
  
  // two histogram fitting
  
  
  /*
  std::vector<TString> fulltag =
    {
     "2b2t",
     //"1b2t",
    };
  */

  std::vector<TString> fulltag;
  
  if ( fit_projection == "tt" || fit_projection == "bb" ) {
    fulltag.push_back("2b2t");
  } else if ( fit_projection == "ttMC" || fit_projection == "bbMC" ) {
    fulltag.push_back("1b2t");
  }

  std::vector<TString> lowtag;

  if ( fit_projection == "tt" ) {
    lowtag.push_back("0b2t");
    lowtag.push_back("1b2t");
  } else if ( fit_projection == "bb" ) {
    lowtag.push_back("2b0t");
    lowtag.push_back("1b0t");
  } else if ( fit_projection == "ttMC" ) {
    lowtag.push_back("2b0t");
    lowtag.push_back("1b0t");
  } else if ( fit_projection == "bbMC" ) {
    lowtag.push_back("2b0t");
    lowtag.push_back("1b0t");
  }
  

  
  for ( TString i: fulltag ) {
    
    TH2 * data_2d = (TH2*) (gDirectory->Get("SB_dat_"+i))->Clone();

    TH2 * bkg1_2d;
    TH2 * bkg2_2d;
    if ( fit_projection == "bb" || fit_projection == "tt" ) {
      bkg1_2d  = (TH2*) (gDirectory->Get("SB_dat_"+lowtag[0]))->Clone();
      bkg2_2d  = (TH2*) (gDirectory->Get("SB_dat_"+lowtag[1]))->Clone();
    } else if ( fit_projection == "bbMC" || fit_projection == "ttMC" ) {
      bkg1_2d  = (TH2*) (gDirectory->Get("SB_dat_"+lowtag[0]))->Clone();
      bkg2_2d  = (TH2*) (gDirectory->Get("SB_bkg_"+lowtag[1]))->Clone();
    } 

    TF1 *ftot = new TF1("ftot",ftotal2,0, 300, 2);
    //TF1 *ftot1bkg = new TF1("ftot1bkg",ftotal1,0, 300, 1);
    if ( fit_projection == "bb" || fit_projection == "bbMC" ) {
      data = (TH1*) data_2d->ProjectionX();
      bkg1 = (TH1*) bkg1_2d->ProjectionX();
      bkg2 = (TH1*) bkg2_2d->ProjectionX();
      // mbb range
      ftot->SetRange(70,200);
      //ftot1bkg->SetRange(70,200);
      //TF1 *ftot = new TF1("ftot",ftotal1,70,200,1);
    } else if ( fit_projection == "tt" || fit_projection == "ttMC" ) {
      data = (TH1*) data_2d->ProjectionY();
      bkg1 = (TH1*) bkg1_2d->ProjectionY();
      bkg2 = (TH1*) bkg2_2d->ProjectionY();
      // mtt range
      ftot->SetRange(20,140);
      //ftot1bkg->SetRange(20,140);
      //TF1 *ftot = new TF1("ftot",ftotal2,20,140,2);
      //TF1 *ftot = new TF1("ftot",ftotal1,20,140,1);
    }

    data->Rebin();
    bkg1->Rebin();
    bkg2->Rebin();
    
    Double_t norm = data->GetMaximum();
    ftot->SetParameters(0.5*norm,0.5*norm);
    //ftot1bkg->SetParameter(0,norm);
    //ftot->SetParLimits(0,.3*norm,norm);
    ftot->SetParName(0,lowtag[0]);
    ftot->SetParName(1,lowtag[1]);
    //ftot1bkg->SetParName(0,lowtag[0]);
    
    // L: log likelihood method (default is chi-square)
    // E: better errors estimation using the Minos technique
    // R: use range specified in function
    // M: improve fit results using IMPROVE from TMinuit
    // 0: (zero) do not plot the result of the fit
    if ( fit_projection == "bb" || fit_projection == "tt" ) {
      data->Fit("ftot","LER0");
    } else if ( fit_projection == "bbMC" || fit_projection == "ttMC" ) {
      data->Fit("ftot1bkg","LER0");
    }
    
    // Drawing the fit
    TCanvas * c1 = new TCanvas("mine");
    data->SetMarkerStyle(8);
    data->Draw("EP");
    //data->SetTitle("Sideband 2b2#tau");
    
    TH1 * draw1 = (TH1 *) bkg1->Clone();
    draw1->SetLineColor(kBlue);
    draw1->Scale(ftot->GetParameter(0));
    draw1->Draw("hist same");

    TH1 * draw2 = (TH1 *) bkg2->Clone();
    draw2->SetLineColor(kGreen);
    draw2->Scale(ftot->GetParameter(1));
    draw2->Draw("hist same");
    
    ftot->Draw("sames");
    ftot->SetTitle("Fit");
    
    TLegend *myleg;
    myleg = c1->BuildLegend(0.62,0.6,0.9,0.9,"","");
    myleg->SetFillStyle(0);
    gStyle->SetOptStat("e");
    gStyle->SetOptFit(1011);
    //gStyle->SetOptStat(0);
    
    c1->Update();
    TPaveStats *stats =(TPaveStats*)c1->GetPrimitive("stats");
    stats->SetName("h1stats");
    stats->SetY1NDC(0.4);
    stats->SetY2NDC(0.6);
    stats->SetX1NDC(0.65);
    stats->SetX2NDC(0.9);
    stats->Draw("NDC");
    //stats->SetTextColor(2);
  
    // Validation!!!
    double bkg1_error, bkg2_error;
    double bkg1_cr_error, bkg2_cr_error;
    double fit1 =  ftot->GetParameter(0);
    double fit2 =  ftot->GetParameter(1);
    double fit1_error = ftot->GetParError(0);
    double fit2_error = ftot->GetParError(1);
    double bkg1_cr_entries = ((TH2*) (gDirectory->Get("CR_dat_"+lowtag[0])))->IntegralAndError(0,60,0,60,bkg1_cr_error,"");
    double bkg2_cr_entries = ((TH2*) (gDirectory->Get("CR_dat_"+lowtag[1])))->IntegralAndError(0,60,0,60,bkg2_cr_error,"");
    
    Double_t ns = ftot->GetParameter(0)* bkg1->GetEntries() + ftot->GetParameter(1)*bkg2->GetEntries();
    cout << "For sideband: " << ns << endl;
    Double_t nv = fit1*bkg1_cr_entries + fit2*bkg2_cr_entries;

    double error_propagated;
    error_propagated = sqrt(
			    fit1*fit1 * bkg1_cr_error*bkg1_cr_error +
			    bkg1_cr_entries*bkg1_cr_entries * fit1_error*fit1_error +
			    fit2*fit2 * bkg2_cr_error*bkg2_cr_error +
			    bkg2_cr_entries*bkg2_cr_entries * fit2_error*fit2_error
			    );
    cout << error_propagated << endl;

    std::cout << std::setprecision(2) << std::fixed;
    cout << "For validation: " << nv << " \\pm " << error_propagated << " &= "
	 << "( " << fit1 << " \\pm " << fit1_error << " ) " << " \\times "
	 << "( " << bkg1_cr_entries << " \\pm " <<  bkg1_cr_error << " ) " << " + "
	 << "( " << fit2 << " \\pm " << fit2_error << " ) " << " \\times "
	 << "( " << bkg2_cr_entries << " \\pm " <<  bkg2_cr_error << " ) " << endl;
	 
    cout << "fit parameters errors " << ftot->GetParError(0) << " " << ftot->GetParError(1) << endl;
    cout << "CR entries errors " << bkg1_cr_error << " " << bkg2_cr_error << endl;

    /*
      Double_t ns = ftot->GetParameter(0)* ((TH2*) (gDirectory->Get("SB_dat_"+lowtag[0])))->GetEntries()
      + ftot->GetParameter(1)*((TH2*) (gDirectory->Get("SB_dat_"+lowtag[1])))->GetEntries();
    cout << "For sideband: " << ns << endl;
    Double_t nv = ftot->GetParameter(0)* ((TH2*) (gDirectory->Get("CR_dat_"+lowtag[0])))->GetEntries()
      + ftot->GetParameter(1)*((TH2*) (gDirectory->Get("CR_dat_"+lowtag[1])))->GetEntries();
    cout << "For validation: " << nv << " &= " <<  ftot->GetParameter(0)
	 << " \\times " << ((TH2*) (gDirectory->Get("CR_dat_"+lowtag[0])))->GetEntries()
	 << " + " <<  ftot->GetParameter(1) << " \\times "
	 << ((TH2*) (gDirectory->Get("CR_dat_"+lowtag[1])))->GetEntries() << endl;
    */
  }
  /*
  new TCanvas();
  bkg1->Draw();
  new TCanvas();
  bkg2->Draw();
  */
  //Draw_th2(h5,gOut_path+"fitting.png");

}

void Draw_th2(TH2F * h2, TString out_filename = "")
{
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  TCanvas * c1 = new TCanvas();
  c1->SetCanvasSize(1500,1500);
  //c1->SetLeftMargin(0.12);
  c1->SetRightMargin(0.15);

  /*
  h2->GetXaxis()->CenterTitle(true);
  h2->GetXaxis()->SetTitleOffset(1.5);
  h2->GetYaxis()->CenterTitle(true);
  h2->GetYaxis()->SetTitleOffset(2.0);
  */

  
  h2->Draw("colz");

  /*
  bkg->SetTitle("Weighted background");
  sig->SetTitle("Weighted signal");
  TLegend *myleg;
  myleg = c1->BuildLegend(0.4,0.6,0.9,0.89,"","");
  myleg->SetFillStyle(0);
  */
  if ( out_filename != "" ) {
    c1->SaveAs(out_filename);
  }
}
